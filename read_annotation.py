from pathlib import Path
import argparse
import json
import numpy as np


def get_features(filename):
    data = json.load(open(filename))
    print("Total objects: ", len(data["objects"]))
    visited, X, y = [], [], []

    for instance in data["objects"]:
        if instance["description"] and instance["classTitle"] == "Id Of Pole":
            order = int(instance["description"])
            if order not in visited:
                points = instance["points"]["exterior"]
                # Taking mean as boxes(polygons) have variiable length of points
                X.append(np.mean(points, axis=0))
                # Orders are: 0, 1, 2, ...
                y.append(order-1)
                visited.append(order)
    X = np.array(X)
    y = sorted(y)
    if len(X) <= 1:
        # Only 1 object, no need to order
        return None, None
    y_mat = np.zeros((len(y), len(y)))
    for val in y[:-1]:
        y_mat[val][val+1] = 1
        y_mat[val+1][val] = 2
    y = []
    for i in range(len(y_mat)):
        y.extend(np.concatenate(
            [
                y_mat[i][0:i],
                y_mat[i][i+1:]
            ]
        ))
    return X, y


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--filename', required=True)
    args = parser.parse_args()
    features, labels = get_features(args.filename)
    # get_features(args.filename)
    print(features)
    print(labels)


if __name__ == "__main__":
    main()


